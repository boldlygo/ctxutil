package ctxutil_test

import (
	"context"
	"os"
	"syscall"
	"testing"
	"time"

	"gitlab.com/boldlygo/ctxutil"
)

func TestWithSignal(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	type args struct {
		ctx context.Context
		sig []os.Signal
	}

	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"sent",
			args{
				ctx,
				[]os.Signal{syscall.SIGUSR1},
			},
			true,
		},
		{
			"unsent",
			args{
				ctx,
				[]os.Signal{syscall.SIGUSR2},
			},
			false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			var got bool
			f := func(sig os.Signal) { got = true }
			ctx, cancel := ctxutil.WithSignal(tt.args.ctx, f, tt.args.sig...)
			defer cancel()
			syscall.Kill(syscall.Getpid(), syscall.SIGUSR1)
			<-ctx.Done()
			if got != tt.want && tt.want {
				t.Errorf("ctxutil.WithSignal() signal canceled Context: got=%t want=%t", got, tt.want)
			}
		})
	}
}
