// Package ctxutil provides Context helpers.
package ctxutil

import (
	"context"
	"os"
	"os/signal"
)

// WithSignal acts like context.WithCancel, but the returned Context is also canceled when one of the passed signals is
// received. Function f, if not nil, is called before canceling the Context.
func WithSignal(ctx context.Context, f func(os.Signal), sig ...os.Signal) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancel(ctx)

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, sig...)

	go func() {
		select {
		case <-ctx.Done():
		case s := <-ch:
			if f != nil {
				f(s)
			}

			cancel()
		}

		signal.Stop(ch)
		close(ch)
	}()

	return ctx, cancel
}
